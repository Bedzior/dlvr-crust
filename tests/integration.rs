#[cfg(test)]
mod integration {
    use dlvr_crust::{self, parsing_errors::ParsingError};
    use test_case::test_case;

    #[test_case("* * * * * /usr/bin/find", r#"
minute         0 1 2 3 4 5 6 7 8 9 10 11 12 13 14 15 16 17 18 19 20 21 22 23 24 25 26 27 28 29 30 31 32 33 34 35 36 37 38 39 40 41 42 43 44 45 46 47 48 49 50 51 52 53 54 55 56 57 58 59
hour           0 1 2 3 4 5 6 7 8 9 10 11 12 13 14 15 16 17 18 19 20 21 22 23
day of month   1 2 3 4 5 6 7 8 9 10 11 12 13 14 15 16 17 18 19 20 21 22 23 24 25 26 27 28 29 30 31
month          1 2 3 4 5 6 7 8 9 10 11 12
day of week    0 1 2 3 4 5 6
command        /usr/bin/find"#
    ; "All asterisks expand to full output")]
    fn full_output(input: &str, output: &str) {
        assert_eq!(
            dlvr_crust::parse_expression(input)
                .expect("Result should be fine")
                .trim(),
            output.trim()
        )
    }

    #[test_case("", "Missing minute expression")]
    #[test_case("*", "Missing hour expression")]
    #[test_case("* *", "Missing day of month expression")]
    #[test_case("* * *", "Missing month expression")]
    #[test_case("* * * *", "Missing day of week expression")]
    #[test_case("* * * * *", "Missing command expression")]
    fn missing_expression(input: &str, output: &str) {
        match dlvr_crust::parse_expression(input) {
            Err(err) => match err {
                ParsingError::MissingSubexpression(msg) => assert_eq!(msg, output),
                _ => assert!(false),
            },
            _ => assert!(false),
        }
    }
    #[test_case(
        "*/2/3 */2 */2 */2 */2 /abc",
        ParsingError::ErrorInSubexpression(3, "Up to one step statement per element is allowed".to_string()); "Extra step in minute expression")
    ]
    fn error_in_subexpression(input: &str, output: ParsingError) {
        match dlvr_crust::parse_expression(input) {
            Err(err) => assert_eq!(err, output),
            _ => assert!(false),
        }
    }
}

#[cfg(test)]
mod executable {
    use std::{
        io::{BufRead, BufReader},
        process::Command,
    };
    use test_case::test_case;

    #[test_case("abc" ; "Well, duh")]
    #[test_case("" ; "Missing minute expression")]
    #[test_case("*" ; "Missing hour expression")]
    #[test_case("* *" ; "Missing day of month expression")]
    #[test_case("* * *" ; "Missing month expression")]
    #[test_case("* * * *" ; "Missing day of week expression")]
    #[test_case("* * * * *" ; "Missing command expression")]
    fn executable_with_bad_input(input: &str) {
        let status = Command::new("cargo")
            .arg("run")
            .arg(format!("{}", input))
            .status()
            .expect("Failed to execute process");

        assert!(!status.success());
    }

    #[test_case("a * * * * /usr/bin/bash", 0; "Malformed minute expression")]
    #[test_case("* b * * * /usr/bin/bash", 2; "Malformed hour expression")]
    #[test_case("* * c * * /usr/bin/bash", 4; "Malformed day of month expression")]
    #[test_case("* * * d * /usr/bin/bash", 6; "Malformed month expression")]
    #[test_case("* * * * e /usr/bin/bash", 8; "Malformed day of week expression")]
    #[test_case("1-4,15-20,25-30,56-59 * 5 */2 e /usr/bin/bash", 30; "Malformed day of week expression with others being longer")]
    #[test_case("*/2/3 */2 */2 */2 */2 /abc", 3; "Extra step in minute expression")]
    fn executable_with_locatable_bad_input(input: &str, location: usize) {
        let output = Command::new("cargo")
            .arg("run")
            .arg(format!("{}", input))
            .output()
            .expect("Command execution error");
        let err = BufReader::new(&*output.stderr);
        let mut found = false;
        err.lines().for_each(|line| {
            let str = line.unwrap();
            if let Some(index) = str.find('^') {
                found = true;
                assert_eq!(index, location);
            }
        });

        assert!(found);
    }

    #[test_case("* * * * * /usr/bin/find" ; "All subexpressions as asterisks")]
    #[test_case("1,2 1,2 1,2 1,2 1,2 /usr/bin/find" ; "All subexpressions with commas")]
    #[test_case("1-3 1-3 1-3 1-3 1-3 /usr/bin/find" ; "All subexpressions with dashes")]
    #[test_case("1-3,4-6 1-3,4-6 1-3,4-6 1-3,4-6 1-3,4-6 /usr/bin/find" ; "All subexpressions with commas and dashes")]
    #[test_case("1-3/2,40-50/3 1-4/2,5-23/3 1-3,4-10/2 1-3/2,6-12/3 1-3/1,4-6/2 /usr/bin/find" ; "All subexpressions with commas, dashes and steps")]
    fn executable_with_good_input(input: &str) {
        let status = Command::new("cargo")
            .arg("run")
            .arg(format!("{}", input))
            .status()
            .expect("Failed to execute process");

        assert!(status.success());
    }
}
