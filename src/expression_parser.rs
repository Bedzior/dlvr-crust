use std::cmp;

use crate::expandable::Expandable;
use crate::field_parser::{CronField, CronFieldParser, Slice};
use crate::parsing_errors::ParsingError;

pub struct CronExpressionParser {
    fields: Vec<Box<dyn CronField>>,
}

impl CronExpressionParser {
    #[must_use]
    pub fn from(str: &str) -> Result<impl Expandable, ParsingError> {
        let mut split = str.split_terminator(" ");
        let mut sections = Vec::with_capacity(6);
        let mut current = 0usize;
        for message in vec![
            "Missing minute expression",
            "Missing hour expression",
            "Missing day of month expression",
            "Missing month expression",
            "Missing day of week expression",
            "Missing command expression",
        ] {
            match split.next() {
                Some(string) => {
                    current += str[current..].find(string).unwrap();
                    sections.push(Slice(string, current));
                }
                None => Err(ParsingError::MissingSubexpression(message.to_string()))?,
            }
        }
        let mut sections_it = sections.iter();
        Ok(CronExpressionParser {
            fields: vec![
                CronFieldParser::minute(sections_it.next().unwrap()),
                CronFieldParser::hour(sections_it.next().unwrap()),
                CronFieldParser::day_of_month(sections_it.next().unwrap()),
                CronFieldParser::month(sections_it.next().unwrap()),
                CronFieldParser::day_of_week(sections_it.next().unwrap()),
                CronFieldParser::command(sections_it.next().unwrap()),
            ],
        })
    }
}

impl Expandable for CronExpressionParser {
    fn expand(&self) -> Result<String, ParsingError> {
        let mut max_length = 0usize;
        for field in &self.fields {
            max_length = cmp::max(field.header().len(), max_length);
        }
        let mut lines = Vec::with_capacity(self.fields.capacity());
        for field in &self.fields {
            lines.push(format!(
                "{:w$} {}\n",
                field.header(),
                field.expand()?,
                w = max_length + 2,
            ));
        }

        Ok(lines.concat())
    }
}
