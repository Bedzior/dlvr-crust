use crate::parsing_errors::ParsingError;

pub trait Expandable {
    #[must_use]
    fn expand(&self) -> Result<String, ParsingError>;
}
