use itertools::Itertools;
use std::{collections::HashSet, ops::Range};

use crate::{expandable::Expandable, parsing_errors::ParsingError};

pub trait Header {
    #[must_use]
    fn header(&self) -> &str;
}

enum CronFieldType {
    Minute(),
    Hour(),
    DayOfMonth(),
    Month(),
    DayOfWeek(),
    Command(),
}

impl Header for CronFieldType {
    fn header(&self) -> &str {
        match self {
            CronFieldType::Minute() => "minute",
            CronFieldType::Hour() => "hour",
            CronFieldType::DayOfMonth() => "day of month",
            CronFieldType::Month() => "month",
            CronFieldType::DayOfWeek() => "day of week",
            CronFieldType::Command() => "command",
        }
    }
}

trait Bounds {
    #[must_use]
    fn bounds(&self) -> Option<(u32, u32)>;
}

impl Bounds for CronFieldType {
    fn bounds(&self) -> Option<(u32, u32)> {
        match self {
            CronFieldType::Minute() => Some((0, 59)),
            CronFieldType::Hour() => Some((0, 23)),
            CronFieldType::DayOfMonth() => Some((1, 31)),
            CronFieldType::Month() => Some((1, 12)),
            CronFieldType::DayOfWeek() => Some((0, 6)),
            CronFieldType::Command() => None,
        }
    }
}

impl CronFieldType {
    #[must_use]
    fn val(&self, val: &str) -> Result<u32, ParsingError> {
        let parsed = val.parse::<u32>();
        match parsed {
            Ok(i) => Ok(i),
            Err(_) => match self {
                CronFieldType::DayOfWeek() => {
                    if let Some(index) = vec!["SUN", "MON", "TUE", "WED", "THU", "FRI", "SAT"]
                        .iter()
                        .position(|&v| v == val)
                    {
                        Ok(index as u32)
                    } else {
                        Err(ParsingError::ErrorInSubexpression(
                            0usize,
                            format!("Could not parse {} value", self.header()),
                        ))
                    }
                }
                CronFieldType::Month() => {
                    if let Some(index) = vec![
                        "JAN", "FEB", "MAR", "APR", "MAY", "JUN", "JUL", "AUG", "SEP", "OCT",
                        "NOV", "DEC",
                    ]
                    .iter()
                    .position(|&v| v == val)
                    {
                        Ok(index as u32 + 1)
                    } else {
                        Err(ParsingError::ErrorInSubexpression(
                            0usize,
                            format!("Could not parse {} value", self.header()),
                        ))
                    }
                }
                _ => Err(ParsingError::ErrorInSubexpression(
                    0usize,
                    format!("Could not parse {} value", self.header()),
                )),
            },
        }
    }
}

pub trait CronField: Header + Expandable {}

#[derive(Clone)]
pub struct Slice<'a>(pub &'a str, pub usize);

pub struct CronFieldParser {
    field_type: CronFieldType,
    expression_slice: String,
    start_index: usize,
}

// No pub beyond this point
#[must_use]
fn create_dyn_field(field_type: CronFieldType, expression_slice: &Slice) -> Box<dyn CronField> {
    Box::new(CronFieldParser {
        field_type,
        expression_slice: expression_slice.0.to_string(),
        start_index: expression_slice.1,
    })
}

impl CronFieldParser {
    #[must_use]
    pub fn minute(expression_slice: &Slice) -> Box<dyn CronField> {
        create_dyn_field(CronFieldType::Minute(), expression_slice)
    }

    #[must_use]
    pub fn hour(expression_slice: &Slice) -> Box<dyn CronField> {
        create_dyn_field(CronFieldType::Hour(), expression_slice)
    }

    #[must_use]
    pub fn day_of_month(expression_slice: &Slice) -> Box<dyn CronField> {
        create_dyn_field(CronFieldType::DayOfMonth(), expression_slice)
    }

    #[must_use]
    pub fn month(expression_slice: &Slice) -> Box<dyn CronField> {
        create_dyn_field(CronFieldType::Month(), expression_slice)
    }

    #[must_use]
    pub fn day_of_week(expression_slice: &Slice) -> Box<dyn CronField> {
        create_dyn_field(CronFieldType::DayOfWeek(), expression_slice)
    }

    #[must_use]
    pub fn command(expression_slice: &Slice) -> Box<dyn CronField> {
        create_dyn_field(CronFieldType::Command(), expression_slice)
    }
}

impl Header for CronFieldParser {
    fn header(&self) -> &str {
        self.field_type.header()
    }
}
impl Bounds for CronFieldParser {
    fn bounds(&self) -> Option<(u32, u32)> {
        self.field_type.bounds()
    }
}
impl CronField for CronFieldParser {}

impl Expandable for CronFieldParser {
    fn expand(&self) -> Result<String, ParsingError> {
        if let CronFieldType::Command() = self.field_type {
            // Command field is trivial
            return Ok(self.expression_slice.to_string());
        }

        let mut expanded = HashSet::new(); // to ensure unique values

        for subexpression in self.expression_slice.split(",") {
            let mut arr = subexpression.split_terminator("/");
            match arr.next() {
                Some(range) => match self.parse_range(range) {
                    Ok(bounds) => {
                        let step = match arr.next() {
                            Some(v) => match v.parse::<usize>() {
                                Ok(val) => val,
                                Err(_) => Err(ParsingError::ErrorInSubexpression(
                                    self.start_index + subexpression.find("/").unwrap() + 1,
                                    "Step value is not a number".to_string(),
                                ))?,
                            },
                            None => 1usize,
                        };
                        if let Some(_) = arr.next() {
                            let at = subexpression.find("/").unwrap() + 1;
                            let index = subexpression[at..].find("/").map(|i| at + i).unwrap();
                            Err(ParsingError::ErrorInSubexpression(
                                self.start_index + index,
                                "Up to one step statement per element is allowed".to_string(),
                            ))?;
                        }
                        expanded.extend(
                            Range {
                                start: bounds.0,
                                end: bounds.1,
                            }
                            .step_by(step),
                        );
                    }
                    Err(e) => Err(ParsingError::bubble(e, self.start_index))?,
                },
                None => Err(ParsingError::ErrorInSubexpression(
                    self.start_index,
                    format!("Missing {} expression", self.header()),
                ))?,
            }
        }
        Ok(expanded.iter().sorted().join(" "))
    }
}

impl CronFieldParser {
    #[must_use]
    fn parse_range(&self, str: &str) -> Result<(u32, u32), ParsingError> {
        let bounds = match str == "*" {
            true => self.bounds().unwrap(),
            false => match str.contains("-") {
                true => {
                    let mut split = str.split_terminator("-");
                    let first = match split.next() {
                        Some(a) => self.field_type.val(a),
                        None => Err(ParsingError::ErrorInSubexpression(
                            0usize,
                            "Missing value before dash".to_string(),
                        )),
                    };
                    let second = match split.next() {
                        Some(a) => self.field_type.val(a),
                        None => Err(ParsingError::ErrorInSubexpression(
                            str.find('-').unwrap() + 1,
                            "Missing value after dash".to_string(),
                        )),
                    };
                    if let Err(parsing) = first {
                        return Err(parsing);
                    }
                    if let Err(parsing) = second {
                        return Err(ParsingError::bubble(parsing, str.find('-').unwrap()));
                    }
                    (first.unwrap(), second.unwrap())
                }
                false => match self.field_type.val(str) {
                    Ok(value) => (value, value),
                    Err(error) => Err(error)?,
                },
            },
        };
        if bounds.0 < self.bounds().unwrap().0 {
            Err(ParsingError::ErrorInSubexpression(
                0usize,
                format!("Lower value below {} bounds", self.header()),
            ))?;
        }
        if bounds.0 > self.bounds().unwrap().1 {
            Err(ParsingError::ErrorInSubexpression(
                0usize,
                format!("Value beyond {} bounds", self.header()),
            ))?;
        }
        if bounds.1 > self.bounds().unwrap().1 {
            Err(ParsingError::ErrorInSubexpression(
                str.find('-').unwrap() + 1,
                format!("Highest value exceeds {} bounds", self.header()),
            ))?;
        }
        if bounds.0 > bounds.1 {
            Err(ParsingError::ErrorInSubexpression(
                0,
                "Lower bound is greater than upper".to_string(),
            ))?;
        }
        Ok((bounds.0, bounds.1 + 1))
    }
}

#[cfg(test)]
mod unit_test {
    use super::*;
    use test_case::test_case;

    #[test_case("60-70", ParsingError::ErrorInSubexpression(0, "Value beyond minute bounds".to_string()); "Out of range")]
    #[test_case("-", ParsingError::ErrorInSubexpression(0, "Could not parse minute value".to_string()); "Just a dash")]
    #[test_case("/", ParsingError::ErrorInSubexpression(0, "Could not parse minute value".to_string()); "Just a divisor")]
    #[test_case("", ParsingError::ErrorInSubexpression(0, "Missing minute expression".to_string()); "Just empty")]
    #[test_case("60", ParsingError::ErrorInSubexpression(0, "Value beyond minute bounds".to_string()); "Out of bounds")]
    #[test_case("1/a", ParsingError::ErrorInSubexpression(2, "Step value is not a number".to_string()); "Wacky step value")]
    fn parser_with_erroneous_input_returns_error(bad_input: &str, expected_err: ParsingError) {
        let parser = CronFieldParser {
            field_type: CronFieldType::Minute(),
            expression_slice: bad_input.to_string(),
            start_index: 0usize,
        };
        let result = parser.expand();
        assert!(result.is_err());
        assert_eq!(result.unwrap_err(), expected_err);
    }

    #[test_case("SUN", "0"; "Sunday")]
    #[test_case("MON", "1"; "Monday")]
    #[test_case("TUE", "2"; "Tuesday")]
    #[test_case("WED", "3"; "Wednesday")]
    #[test_case("THU", "4"; "Thursday")]
    #[test_case("FRI", "5"; "Friday")]
    #[test_case("SAT", "6"; "Saturday")]
    #[test_case("MON-FRI", "1 2 3 4 5"; "Monday to Friday")]
    #[test_case("SAT,SUN", "0 6"; "Weekend")]
    #[test_case("*/2", "0 2 4 6"; "Every 2nd day")]
    #[test_case("MON-THU/2", "1 3"; "Every second day until Thursday")]
    fn day_of_week_parser_with_string_input_expands_to_int(expr: &str, expected: &str) {
        let parser = CronFieldParser::day_of_week(&Slice(expr, 0));
        let output = parser.expand();
        assert!(output.is_ok());
        assert_eq!(output.unwrap(), expected);
    }

    #[test_case("JAN", "1"; "January")]
    #[test_case("FEB", "2"; "February")]
    #[test_case("MAR", "3"; "March")]
    #[test_case("APR", "4"; "April")]
    #[test_case("MAY", "5"; "May")]
    #[test_case("JUN", "6"; "June")]
    #[test_case("JUL", "7"; "July")]
    #[test_case("AUG", "8"; "August")]
    #[test_case("SEP", "9"; "September")]
    #[test_case("OCT", "10"; "October")]
    #[test_case("NOV", "11"; "November")]
    #[test_case("DEC", "12"; "December")]
    #[test_case("JAN-DEC", "1 2 3 4 5 6 7 8 9 10 11 12"; "Whole year")]
    #[test_case("APR,AUG,DEC", "4 8 12"; "Every 4th month, listed")]
    #[test_case("JAN-DEC/4", "1 5 9"; "Every 4th month, starting from January")]
    fn month_parser_with_string_input_expands_to_int(expr: &str, expected: &str) {
        let parser = CronFieldParser::month(&Slice(expr, 0));
        assert_eq!(parser.expand().unwrap(), expected);
    }
}
