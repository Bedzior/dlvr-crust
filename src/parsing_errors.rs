#[derive(Debug, Clone, PartialEq)]
pub enum ParsingError {
    /// An error type for a missing subexpression
    MissingSubexpression(String),
    /// An issue localized in one of the subexpressions
    /// The first argument indicates the index at which the parsing error occurred
    ErrorInSubexpression(usize, String),
}

impl ParsingError {
    pub fn bubble(e: ParsingError, offset: usize) -> ParsingError {
        match e {
            ParsingError::MissingSubexpression(_) => e,
            ParsingError::ErrorInSubexpression(i, msg) => {
                ParsingError::ErrorInSubexpression(i + offset, msg)
            }
        }
    }
}
