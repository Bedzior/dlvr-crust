use std::env;
pub mod parsing_errors;

use dlvr_crust::parsing_errors::ParsingError;

fn main() {
    let args: Vec<String> = env::args().collect();

    if args.len() != 2 {
        eprintln!("Expecting a single input parameter");
        std::process::exit(exitcode::USAGE);
    } else {
        let expression = &args[1];
        match dlvr_crust::parse_expression(expression) {
            Ok(output) => {
                println!("{}", output);
                std::process::exit(exitcode::OK);
            }
            Err(err) => match err {
                ParsingError::MissingSubexpression(message) => {
                    eprintln!("{}\n{}", expression, message);
                    std::process::exit(exitcode::USAGE);
                }
                ParsingError::ErrorInSubexpression(index, message) => {
                    eprintln!("{}\n{:->#w$}\n{}", expression, "^", message, w = index + 1);
                    std::process::exit(exitcode::USAGE);
                }
            },
        }
    }
}
