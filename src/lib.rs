mod expandable;
mod expression_parser;
mod field_parser;
pub mod parsing_errors;

use expandable::Expandable;
use expression_parser::CronExpressionParser;
use parsing_errors::ParsingError;

/// Sample usage:
/// ```
/// let input = "*/15 0 1,15 * 1-5 /usr/bin/find";
/// # let expected_output = "minute         0 15 30 45\nhour           0\nday of month   1 15\nmonth          1 2 3 4 5 6 7 8 9 10 11 12\nday of week    1 2 3 4 5\ncommand        /usr/bin/find";
/// let output = dlvr_crust::parse_expression(input).unwrap();
/// # assert_eq!(output.trim(), expected_output.trim());
/// println!("{}", output)
/// ```
#[must_use]
pub fn parse_expression(expression: &str) -> Result<String, ParsingError> {
    CronExpressionParser::from(expression)?.expand()
}
